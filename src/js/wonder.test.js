const Wonder = require('./wonder.js');

test("Output of Wonder function", () => {
    expect(Wonder()).toBe("This should work.");
});

test("Returning nothing", () => {
    expect(Wonder()).toBe("This should not work.");
});

